#include <stdio.h>
#include <unistd.h>
#include <time.h>

int main()
{
    struct timespec start, finish;
    double res = 0;
    int maxLoop = 100;
    for(int cpt = 0; cpt < maxLoop; cpt ++)
    {
        double sec, nano;
        clock_gettime(CLOCK_MONOTONIC, &start);
        //sleep(1);
        clock_gettime(CLOCK_MONOTONIC, &finish);

        sec = (finish.tv_sec - start.tv_sec);
        nano = finish.tv_nsec - start.tv_nsec;

        printf("%.10f\n", sec + nano / 1000000000);
        res += sec + nano / 1000000000;
    }
    printf("MOY: %.10f\n", res / maxLoop);


    return 0;
}