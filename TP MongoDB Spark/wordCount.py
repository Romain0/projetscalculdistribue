#!/usr/bin/env python
from pyspark import SparkContext

def main (uri):
    sc = SparkContext()
    try:
        doc = sc.textFile(uri)
        counts = doc.flatMap(lambda line: line.split(" "))
        counts = counts.map(lambda word: (word, 1))
        counts = counts.reduceByKey(lambda a, b: a + b)
        counts = counts.sortBy(lambda c: -c[1])
        print(counts.collect())
    except IOError:
        print("An error is occured, please try again.")
