#!/usr/bin/env python
from pyspark import SparkContext

def array_compare(first_array, second_array):
    res = 0
    for value in first_array:
        if value in second_array:
            res += 1
    return res

def main (uri):
    sc = SparkContext()
    try:            
        doc = sc.textFile(uri)
        doc = doc.flatMap(lambda line: line.split(" "))
        doc.map(lambda x: ([0], x[1:]))

        multiple = doc.cartesian(doc)
        multiple.map(lambda entry: ((entry[0][0], entry[1][0]), array_compare(entry[0][1], entry[1][1])))

        print(multiple.collect())
    except IOError:
        print("An error is occured, please try again.")