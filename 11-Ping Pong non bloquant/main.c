#include <stdio.h>
#include <mpi.h>

int main(int argc, char **argv)
{
    MPI_Request request = MPI_REQUEST_NULL;
    MPI_Status status;
    int rank, tag = 42, datas = 1337, datar;
    MPI_Init(&argc, &argv);
    // Récupère le numéro du processus courrant dans le communicateur global
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if(rank == 0)
    {
        datas = 1;
        // Envoie les données à un destinataire donné
        MPI_Isend(&datas, 1, MPI_INT, 1, tag, MPI_COMM_WORLD, &request);
        // Reçoit des données dans un buffer
        MPI_Irecv(&datar, 1, MPI_INT, 1, tag, MPI_COMM_WORLD, &request);
    }
    else
    {
        MPI_Irecv(&datar, 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &request);
        datar = 0;
        MPI_Isend(&datar, 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &request);
    }

    MPI_Wait(&request, &status);
    if(rank == 0)
    {
        printf("%d : M sent %d, recv %d\n", rank, datas, datar);
    }
    else
    {
        printf("%d : S recv %d\n", rank, datar);
    }

    MPI_Finalize();
    return 0;
}
