#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

struct thread_arg {
  int id;
  int time;
};

void *dispHello(void *arg) {
    // recuperation des donnees
    struct thread_arg * input = arg;
    int id = input->id;
    int time = input->time;
    // affichage des ids en entree
    printf("%d => ", id);
    // id²
    int res = id * id;
    printf("%d : ", res);

    printf("Hello World !\n");
    sleep(time);
    printf("Welcome & Enjoy !\n");
    sleep(10); // sleep 10 sec

    // affichage des ids en sortie
    printf("%d : ", id);

    printf("Bye !\n");

    return (void *) res;
}

int main() {
    int cpt;
    int limit = 10;
    int min = 1;
    int max = 5;
    struct thread_arg args[limit];
    pthread_t th[limit];
    srand((unsigned) time(NULL));

    for (cpt=0; cpt < limit; cpt++) {
        // affectation des ids
        args[cpt].id = cpt;
        args[cpt].time = (rand() % (max - min + 1)) + min;
        // creation des threads
        pthread_create(&th[cpt], NULL, dispHello, &args[cpt]);
    }

    for (cpt=0; cpt < limit; cpt++) {
        // fin des threads
        int returnValue;
        pthread_join(th[cpt], (void **)&returnValue);
        printf("%d\n ", returnValue);
    }
    return 0;
}