#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

int num = 0;

void *increment() {
    for(int cpt = 0; cpt < 1000000; cpt ++) {
        num += 1;
    }
    return NULL;
}

int main() {
    int cpt;
    int limit = 10;
    pthread_t th[limit];

    for (cpt=0; cpt < limit; cpt++) {
        // creation des threads
        pthread_create(&th[cpt], NULL, increment, NULL);
    }

    for (cpt=0; cpt < limit; cpt++) {
        // fin des threads
        pthread_join(th[cpt], NULL);
    }

    printf("%d\n", num);

    return 0;
}