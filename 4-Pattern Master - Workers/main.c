#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

struct thread_arg
{
  int id;
  int *arr;
  int firstElement;
  int nbElements;
};

void *increment(void *arg)
{
    struct thread_arg *threadArg = arg;
    int sumElements = 0;

    for(int cpt = threadArg->firstElement; cpt < (threadArg->firstElement + threadArg->nbElements); cpt ++)
    {
        printf("id => %d, ", threadArg->id);
        printf("value: %d\n", threadArg->arr[cpt]);
        // somme des valeures
        sumElements += threadArg->arr[cpt];
    }

    return (void *) sumElements;
    //return NULL;
}

int main()
{
    int cpt;
    int nbThread = 10;
    int arrayLength = 10;
    int intArray[arrayLength];
    pthread_t th[nbThread];
    struct thread_arg threadArg[nbThread];
    srand((unsigned) time(NULL));

    // set random value to array
    for (cpt = 0; cpt < arrayLength; cpt ++)
    {
        intArray[cpt] = rand() % 100;
    }

    int nbArgs = arrayLength / nbThread;
    for (cpt = 0; cpt < nbThread; cpt ++)
    {
        threadArg[cpt].id = cpt;
        threadArg[cpt].arr = intArray; // le nom du tableau seul, est un pointeur vers le 1er element
        threadArg[cpt].firstElement = cpt * nbArgs;
        threadArg[cpt].nbElements = nbArgs;

        // creation des threads
        pthread_create(&th[cpt], NULL, increment, &threadArg[cpt]);
    }

    int somme = 0;
    for (cpt=0; cpt < nbThread; cpt ++)
    {
        int res = 0;
        // fin des threads
        pthread_join(th[cpt], (void **)&res);
        somme += res;
    }
        printf("%d\n ", somme);

    return 0;
}