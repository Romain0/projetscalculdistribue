#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <pthread.h>

int main(int argc, char **argv)
{
    MPI_Request request = MPI_REQUEST_NULL;
    MPI_Status status;
    srand((unsigned) time(NULL));
    int rank, buf = 0;
    MPI_Init(&argc, &argv);
    // Récupère le numéro du processus courrant dans le communicateur global
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if(rank == 0)
    {
        buf = rand() % 100;

        printf("%d : broadcast %d\n", rank, buf);
    }

    MPI_Ibcast(&buf, 1, MPI_INT, 0, MPI_COMM_WORLD, &request);

    MPI_Wait(&request, &status);

    printf("%d : recv %d\n", rank, buf);
    
    MPI_Finalize();
    
    return 0;
}
