#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

void *dispHello() {
    printf("Hello World !\n");
    sleep(10); // sleep 10 sec
    printf("Bye !\n");
    return NULL;
}

// ne pas oublier gcc main.c -lpthread
int main() {
    int cpt;
    int limit = 10;

    pthread_t th[limit];

    for (cpt=0; cpt < limit; cpt++) {
        // creation des threads
        pthread_create(&th[cpt], NULL, dispHello, NULL);
    }

    for (cpt=0; cpt < limit; cpt++) {
        // fin des threads
        pthread_join(th[cpt], NULL);
    }
    return 0;
}