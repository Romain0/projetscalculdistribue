#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <pthread.h>

int main(int argc, char **argv)
{
    srand((unsigned) time(NULL));
    float global_sum, local_sum = rand() % 100;
    int rank;
    MPI_Init(&argc, &argv);
    // Récupère le numéro du processus courrant dans le communicateur global
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    printf("Local sum for process %d - %f\n", rank, local_sum);

    MPI_Reduce(&local_sum, &global_sum, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);

    if (rank == 0) {
        printf("Total sum = %f\n", global_sum);
    }           
    
    MPI_Finalize();
    
    return 0;
}
