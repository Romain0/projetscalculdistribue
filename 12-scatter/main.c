#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <pthread.h>

int main(int argc, char **argv)
{
    int buffer[4] = {0, 100, 200, 300};
    srand((unsigned) time(NULL));
    int rank;
    MPI_Init(&argc, &argv);
    // Récupère le numéro du processus courrant dans le communicateur global
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int my_value;
    if (rank == 0) {
        MPI_Scatter(buffer, 1, MPI_FLOAT, &my_value, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
    } else {
        MPI_Scatter(NULL, 1, MPI_FLOAT, &my_value, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
    }

    printf("Process %d received value = %d.\n", rank, my_value * my_value);

    MPI_Finalize();
    
    return 0;
}
