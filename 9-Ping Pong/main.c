#include <stdio.h>
#include <mpi.h>

int main(int argc, char **argv)
{
    int rank, tag = 42, datas = 1337, datar;
    MPI_Init(&argc, &argv);
    // Récupère le numéro du processus courrant dans le communicateur global
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if(rank == 0)
    {
        datas = 1;
        // Envoie les données à un destinataire donné
        MPI_Send(&datas, 1, MPI_INT, 1, tag, MPI_COMM_WORLD);
        // Reçoit des données dans un buffer
        MPI_Recv(&datar, 1, MPI_INT, 1, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        // Ainsi, datas <=> datar
        printf("%d : M sent %d, recv %d\n", rank, datas, datar);
    }
    else
    {
        MPI_Recv(&datar, 1, MPI_INT, 0, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("%d : S recv %d\n", rank, datar);
        datar = 0;
        MPI_Send(&datar, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
    }
    
    MPI_Finalize();
    return 0;
}
