#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

int num = 0;
pthread_spinlock_t  lock;

void *increment() {
    pthread_spin_lock(&lock);

    for(int cpt = 0; cpt < 1000000; cpt ++) {
        num += 1;
    }

    pthread_spin_unlock(&lock);
    
    return NULL;
}

int main() {
    struct timespec start, finish;
    double diff;
    int cpt;
    int limit = 10;
    pthread_t th[limit];

    pthread_spin_init(&lock, 0);

    clock_gettime(CLOCK_MONOTONIC, &start);

    for (cpt=0; cpt < limit; cpt++) {
        // creation des threads
        pthread_create(&th[cpt], NULL, increment, NULL);
    }

    for (cpt=0; cpt < limit; cpt++) {
        // fin des threads
        pthread_join(th[cpt], NULL);
    }
    
    clock_gettime(CLOCK_MONOTONIC, &finish);

    diff = finish.tv_nsec - start.tv_nsec; 

    pthread_spin_destroy(&lock);

    printf("%d\n", num);

    printf("Time execution : %f\n", diff / 1000000000);

    return 0;
}
